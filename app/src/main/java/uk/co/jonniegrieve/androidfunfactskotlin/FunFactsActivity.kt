package uk.co.jonniegrieve.androidfunfactskotlin


import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import java.util.*

/**
 * Created by Jonnie on 19/10/2017.
 */

class FunFactsActivity : AppCompatActivity() {

    private val TAG = FunFactsActivity::class.java.simpleName

    private val factBook = FactBook()
    private val colorWheel = ColorWheel()

    //declare our view variables
    private var factsTextView: TextView? = null;
    private var showFactsButton: Button? = null;
    private var relativeLayout: RelativeLayout? = null;




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fun_facts)



            //assign views
            factsTextView = findViewById(R.id.factsTextView)
            showFactsButton = findViewById(R.id.showFactsButton)
            relativeLayout = findViewById(R.id.relativeLayout)

            showFactsButton!!.setOnClickListener {
                val fact = factBook.getFact()
                val colors = colorWheel.getColor();
                //val colour = colours.getColour()
                //Update the screen with new fact
                factsTextView!!.text = fact;
                relativeLayout!!.setBackgroundColor(colors)
                showFactsButton!!.setTextColor(colors)
        }


        Log.d(TAG, "Method called from OnCreate");
        //Toast.makeText(this, "Oncreate method was called", LENGTH_LONG).show()
    }


}